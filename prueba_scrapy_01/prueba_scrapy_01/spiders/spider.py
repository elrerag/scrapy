import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractor import LinkExtractor
from prueba_scrapy_01.items import PruebaScrapy01Item
from scrapy.exceptions import CloseSpider

class MercadoLibreMexico(CrawlSpider):
    name = 'mercado_libre_mexico'
    item_count = 0
    allowed_domain = ['www.mercadolibre.com.mx']
    start_urls = ['https://listado.mercadolibre.com.mx/impresoras#D[A:impresoras]'] # donde va a empezar a hacer el scrapy

    rules = {
        Rule(LinkExtractor(allow=(), restrict_xpaths=('//li[@class="pagination__next"]/a'))),
        Rule(LinkExtractor(allow=(), restrict_xpaths=('//h2[@class="item__title list-view-item-title"]')),
             callback='parse_item', follow= False)

    }

    def parse_item(self, response):
        ml_item = PruebaScrapy01Item()

        #informacion de producto
        ml_item['titulo'] = response.xpath('normalize-space(//h1[@class="item-title__primary "]/text())').extract()
        ml_item['folio'] = response.xpath('normalize-space(//span[@class="item-info__id-number"]/text())').extract()
        ml_item['precio'] = response.xpath('normalize-space(//span[@class="price-tag-symbol"]/@content)').extract()
        ml_item['condicion'] = response.xpath('normalize-space(//div[@class="item-conditions"]/text())').extract()
        ml_item['envio'] = response.xpath('normalize-space(//p[@data-block="shipping-method-title"]/text())').extract()
        ml_item['ubicacion'] = response.xpath('normalize-space(//p[@class="card-description text-light"]/text())').extract()
        ml_item['opiniones'] = response.xpath('normalize-space(//span[@class="review-summary-average"]/text())').extract()
        ml_item['ventas_producto'] = response.xpath('normalize-space(//div[@class="item-conditions"]/text())').extract()

        #informacion del vendedor
        ml_item['vendedor_url'] = response.xpath('normalize-space(/html/body/main/div/div/div[1]/div[2]/div[1]/section[2]/a/@href)').extract()
        ml_item['tipo_vendedor'] = response.xpath('normalize-space(/html/body/main/div/div/div[1]/div[2]/div[1]/section[2]/div[3]/p)').extract()
        ml_item['reputacion'] = response.xpath('normalize-space(//div[@class="feedback-title"]/text())').extract()
        ml_item['ventas_vendedor'] = response.xpath('normalize-space(//div[@class="feedback-title"]/text())').extract()

        self.item_count +=1
        if self.item_count > 5:
            raise CloseSpider('item_exceeded')
        yield ml_item
