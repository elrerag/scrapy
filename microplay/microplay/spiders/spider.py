import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractor import LinkExtractor
from microplay.items import MicroplayItem
from scrapy.exceptions import CloseSpider

class MicroplaySpider3ds(CrawlSpider):
	name = 'MicroplaySpider3ds'
	allowed_domain = ['http://www.weplay.cl']
	start_urls = ['http://www.weplay.cl/juegos/juegos3ds.html'] # donde va a empezar a hacer el scrapy	
	
	#recorre el sitio y entra al detalle, el primero es el link de la siguiente página, el segundo es el link al producto
	rules = {
		Rule(LinkExtractor(allow=(), restrict_xpaths=('//a[@title="Siguiente"]'))),
		Rule(LinkExtractor(allow=(), restrict_xpaths=('//h2[@class="product-name"]/a')),
		callback='parse_item', follow= False)}
	
	def parse_item(self, response):
		ml_item = MicroplayItem()
		
		#informacion de producto
		ml_item['titulo'] = response.xpath('normalize-space( //div[@class="product-name"]/h1/text() )').extract()
		ml_item['precio'] = response.xpath('normalize-space( //span[@class="regular-price"]/span/text() )').extract()
	
		yield ml_item