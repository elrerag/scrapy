import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractor import LinkExtractor
from zmartjuegosps4.items import Zmartjuegosps4Item
from scrapy.exceptions import CloseSpider

class Zmartjuegosps4Spider(CrawlSpider):

	name 			= 'juegosPs4Zmart'
	allowed_domain 	= ['https://www.zmart.cl/Scripts/default.asp']
	start_urls 		= ['https://www.zmart.cl/JuegosPS4'] # donde va a empezar a hacer el scrapy

	rules = {
		Rule(LinkExtractor(allow=(), restrict_xpaths=('//div[@class="ProdDisplayType5_MasProductos"]'))),
		Rule(LinkExtractor(allow=(), restrict_xpaths=('//div[@class="BoxProductoS2_Descripcion"]')),
			callback='parse_item', follow= False)
		}

	def parse_item(self, response):
		ml_item = Zmartjuegosps4Item()

		#informacion de producto
		ml_item['sku'] = response.xpath('normalize-space( //*[@id="imagen_producto"]/div[6]/span[2]/text() )').extract()
		ml_item['nombre'] = response.xpath('normalize-space( //div[@id="ficha_producto_int"]/h1/text() )').extract()
		ml_item['precio'] = response.xpath('normalize-space( //div[@id="PriceProduct"]/text() )').extract()

		yield ml_item
